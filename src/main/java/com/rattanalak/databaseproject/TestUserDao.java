/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rattanalak.databaseproject;

import com.rattanalak.databaseproject.dao.UserDao;
import com.rattanalak.databaseproject.helper.DatabaseHelper;
import com.rattanalak.databaseproject.model.User;

/**
 *
 * @author Rattanalak
 */
public class TestUserDao {

    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        for (User u : userDao.getAll()) {
            System.out.println(u);
        }
        //      User user1 =  userDao.get(2);
        //     System.out.println(user1);

//       User newUser = new User("user3","password",2,"F");
//       User insertedUser = userDao.save(newUser);
//       System.out.println(insertedUser);
//       insertedUser.setGender("M");
//       user1.setGender("F");
//       userDao.update(user1);
//       User upDateUser =  userDao.get(user1.getId());
//      System.out.println(upDateUser);
//      userDao.delete(user1);
//      for (User u : userDao.getAll()) {
//          System.out.println(u); 
//      }

        for (User u : userDao.getAll(" user_name  like 'u%' ", "user_name asc, user_gender desc")) {
            System.out.println(u);
        }
        DatabaseHelper.close();
    }

}
